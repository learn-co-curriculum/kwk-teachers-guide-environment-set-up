## Objectives

1. Welcome students / Introductions
2. Make sure all students are signed up in Learn.co and are able to use the in-browser IDE.
2. Students should explore CLI commands and get familiar with the basic commands
3. Students will practice some basic Ruby, including variables and strings
4. Students should be able to comfortably create Ruby .rb files and be able to execute them in the command line

## Resources

### Learn IDE

* [The In-Browser Learn IDE](http://help.learn.co/the-learn-ide/ide-in-browser)
* [Reference Materials for Learn](http://help.learn.co/the-learn-ide)

### Intro to CLI

* [Reference Sheet for Advanced CLI](https://files.fosswire.com/2007/08/fwunixref.pdf)
* [Tutorial on CLI](https://gist.github.com/aviflombaum/9d6f7448119bae3a24ee)
* [Command Line for Beginners](https://lifehacker.com/5633909/who-needs-a-mouse-learn-to-use-the-command-line-for-almost-anything)

### Intro to Ruby

* [Ruby Doc's Intro to Ruby](http://ruby-doc.org/docs/ruby-doc-bundle/Tutorial/index.html)
* [What is Ruby and Where Did It Come From?](https://github.com/learn-co-curriculum/matz-readme)
* [Intro to IRB](https://github.com/learn-co-curriculum/irb-readme)

### Ruby Variables

* [Video - Intro to Variables](https://www.youtube.com/embed/FsVYkcOoI8Y?rel=0&showinfo=0)
* [Variable Types Readme](https://github.com/learn-co-curriculum/ruby-variable-types-readme)
* [Ruby Variables](http://zetcode.com/lang/rubytutorial/variables/)
* [Ruby Variable Reference](https://en.wikibooks.org/wiki/Ruby_Programming/Syntax/Variables_and_Constants)
* [Ruby Strings](http://ruby-doc.org/core-2.1.2/String.html)
